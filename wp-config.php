<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bussinessdirectory' );

/** Database username */
define( 'DB_USER', 'phpmyadmin' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'In}~NA`0E/BUAQ.uH<sr&vB^m{U/@Ih9#afJ)R]:sy _td-#`qmDa[#./=n+g6`Q' );
define( 'SECURE_AUTH_KEY',  '<wvG 3@xCp78*<DKp.Vy`1y;Q6qu_s!]MR,7;f0e008YHg*qR2<SP(gWNKSlc(]X' );
define( 'LOGGED_IN_KEY',    'd<H$tMeA?XHk[SzFZkj0 3CEjeQXGI~4*;+5p;TeZUCr>>?O7h.Mod,WCW<5>`;|' );
define( 'NONCE_KEY',        'hkOpExgB>zh)ewp_MHS~NL0g&CqZqbfrYyXJ34aK@I9v$#IivkF`>hDo=.?c3Ph$' );
define( 'AUTH_SALT',        '63}$mx5n[RydpxE[]MpW87kgh,x Q4Q4){tHM_R>_3L]l)ssLnpOV5J W$s~(mUe' );
define( 'SECURE_AUTH_SALT', ')i+a|Z#QlP/-(ktExV0p|Xex)IHEIpZ`.t=*2(h`MM@yV9,bXq4b::.vrCdm))G?' );
define( 'LOGGED_IN_SALT',   'Lz^2{b%A& b-2NjfaGbZqIBd`%%wmoT{?}q%l;p]m@VpInl+B3L!_XpFm=D33dzd' );
define( 'NONCE_SALT',       'BM<62CB9K{OnS;|{B-mupxYU~cf#$zZ9tbv?$?_6fO )Z:8DE<OYvlT?kv49[=AI' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';