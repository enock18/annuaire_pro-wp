<?php
/**
 * @package businessdirectory 
 * @version 1.0.
 */
/*
Plugin Name: businessdirectory
Plugin URI: http://wordpress.org/plugins/hello-dolly/
Description: This is not just a plugin, it symbolizes the hope and enthusiasm of an entire generation summed up in two words sung most famously by Louis Armstrong: Hello, Dolly. When activated you will randomly see a lyric from <cite>Hello, Dolly</cite> in the upper right of your admin screen on every page.
Author: Enock MUKOKO
Version: businessdirectory
Author URI: "http://ma.tt/"
*/


//[request]
function request_func( $atts ){

    $servername = "localhost";
    $database = "yourdatabase";
    $username = "yourusername";
    $password = "yourpassword";

    global $wpdb;
    $results = $wpdb->get_results( "SELECT * FROM wp_annuaire", OBJECT );
    // foreach ($results as $result){
    //     echo $result->nom_entreprise;
    //     }
    // Create connection
    //print_r($results);

   $conn = mysqli_connect($servername, $username, $password, $database);
  
    // Check connection
  
    if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
    }
  
    //echo("Connected successfully");
    $res = $conn->query('SELECT * FROM wp_annuaire');
  
    $test = '<table class="center" style="margin-left: auto;
     margin-right: auto; border: 2px solid black ;  box-shadow: 0 0 20px rgba(0, 0, 0, 0.15); ">
       <tr>
       <th style="border: 1px solid black;">Id</th>
         <th style="border: 1px solid black;">Nom Entreprise</th>
         <th style="border: 1px solid black;">Localisation Entreprise</th>
         <th style="border: 1px solid black;">Prenom Contact</th>
         <th style="border: 1px solid black;">Nom Contact</th>
         <th style="border: 1px solid black;">Mail Contact</th>
       </tr>';
       
       while($r= $res->fetch_array(MYSQLI_ASSOC)){
  
      $test = $test . '<tr>
         <td style="border: 1px solid black;">'. $r["id"] .'</td>
         <td style="border: 1px solid black;">' . $r["nom_entreprise"] .'</td>
         <td style="border: 1px solid black;">'. $r["localisation_entreprise"] .'</td>
         <td style="border: 1px solid black;">'.$r["prenom_contact"] .'</td>
         <td style="border: 1px solid black;">'.$r["nom_contact"] .'</td>
         <td style="border: 1px solid black;">'.$r["mail_contact"] .'</td>
       </tr>';
  
    };
    
    $test = $test .'</table>';
    return $test;
  
  }
  add_shortcode( 'request', 'request_func' );
